package utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import genericMethods.CommonMethods;

public class CreateSheetAndStoreDate {
	 private static String[] columns = {"Moive Title", "Year Of Release", "Imdb rating"};
	    private static List<TopMoviesList> top =  new ArrayList<>();
	 // Initializing employees data to insert into the excel file
	    public static void storeData(String Title, String Year,String Rating) {
	       top.add(new TopMoviesList(Title,Year,Rating));
	   }

	    public static void createExcel(String filter) throws IOException, InvalidFormatException {
	    	storeData(null, null, null);
	    	// Create a Workbook
	        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

	        // Create a Sheet
	        Sheet sheet = workbook.createSheet("TopMovies");

	        // Create a Font for styling header cells
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setFontHeightInPoints((short) 14);
	        headerFont.setColor(IndexedColors.GREEN.getIndex());

	        // Create a CellStyle with the font
	        CellStyle headerCellStyle = workbook.createCellStyle();
	        headerCellStyle.setFont(headerFont);

	        // Create a Row
	        Row headerRow = sheet.createRow(0);

	        // Create cells
	        for(int i = 0; i < columns.length; i++) {
	            Cell cell = headerRow.createCell(i);
	            cell.setCellValue(columns[i]);
	            cell.setCellStyle(headerCellStyle);
	        }

	        // Create Other rows and cells with employees data
	        int rowNum = 1;
	        for(TopMoviesList t: top) {
	            Row row = sheet.createRow(rowNum++);

	            row.createCell(0)
	                    .setCellValue(t.getMovieTitle());

	            row.createCell(1)
	                   .setCellValue(t.getReleaseYear());

	            row.createCell(2)
	                    .setCellValue(t.getRating());
	        }

			// Resize all columns to fit the content size
	        for(int i = 0; i < columns.length; i++) {
	            sheet.autoSizeColumn(i);
	        }
	        //Giving path to store sheet in users Download folder
	        String home = System.getProperty("user.home");
	        File file = new File(home+"/Downloads/"+filter+ " " +CommonMethods.fn_GetTimeStamp() +".xlsx");
	        // Write the output to a file
	        FileOutputStream fileOut = new FileOutputStream(file);
	        workbook.write(fileOut);
	        fileOut.close();

	        // Closing the workbook
	        workbook.close();
	    }

	     

	    

}
