package utility;


public class TopMoviesList {
	   private String movieTitle;
	    private String releaseYear;
	    private String rating;
	    public TopMoviesList(String movieTitle, String releaseYear, String rating) {
	        this.movieTitle = movieTitle;
	        this.releaseYear = releaseYear;
	        this.rating = rating;
	    }
		public String getMovieTitle() {
			return movieTitle;
		}
		public void setMovieTitle(String movieTitle) {
			this.movieTitle = movieTitle;
		}
		public String getReleaseYear() {
			return releaseYear;
		}
		public void setReleaseYear(String releaseYear) {
			this.releaseYear = releaseYear;
		}
		public String getRating() {
			return rating;
		}
		public void setRating(String rating) {
			this.rating = rating;
		}
}
	  
