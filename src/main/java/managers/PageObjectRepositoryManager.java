package managers;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class PageObjectRepositoryManager {

	public static WebDriver driver;
	public FileInputStream ipStream;
	public String RepositoryFile;
	public Properties propertyFile = new Properties();
    //function to read object repository and create web elements 
	public By getObjectLocator(String locatorName, String fileName) throws Exception {
		//Load Object Repository
		RepositoryFile = "src//test//resources//ObjectRepository//" + fileName + ".properties";
		ipStream = new FileInputStream(RepositoryFile);
		propertyFile.load(ipStream);
		String locatorProperty = propertyFile.getProperty(locatorName);
		System.out.println(locatorProperty.toString());
		String locatorType = locatorProperty.split(":")[0];
		String locatorValue = locatorProperty.split(":")[1];
		 //System.out.println(locatorValue.toString());
		//By locator = null;
	   //WebElement element_locater = null;

	
		// Updated Code

		if (locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);
		else if (locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if ((locatorType.toLowerCase().equals("classname")) || (locatorType.toLowerCase().equals("class")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("tagname")) || (locatorType.toLowerCase().equals("tag")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("linktext")) || (locatorType.toLowerCase().equals("link")))
			return By.linkText(locatorValue);
		else if (locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if ((locatorType.toLowerCase().equals("cssselector")) || (locatorType.toLowerCase().equals("css")))
			return By.cssSelector(locatorValue);
		else if (locatorType.toLowerCase().equals("xpath"))
			return By.xpath(locatorValue);
		else
			throw new Exception("Locator type '" + locatorType + "' not defined!!");

	}  
}



/*
 * Due to below error not able to use switch so using if else condition Cannot
 * switch on a value of type String for source level below 1.7. Only convertible
 * int values or enum variables are permitted
 */

/*
 * public enum LocaterType {
 *  id,Xpath,name }
 *   switch(locatorType) {
 *    case "Id":
 *    locator = By.id(locatorValue); 
 *    break; 
 *    case "Name": locator =By.name(locatorValue); 
 *    break; 
 *    case "CssSelector":
 *    locator = By.cssSelector(locatorValue);
 *    break; 
 *    case "LinkText":
 *    locator = By.linkText(locatorValue);
 *    break;
 *    case "PartialLinkText": 
 *    locator =By.partialLinkText(locatorValue); 
 *    break; 
 *    case "TagName": 
 *    locator =By.tagName(locatorValue);
 *    break; 
 *    case "Xpath":
 *    locator = By.xpath(locatorValue); 
 *    break; } }}
 */
