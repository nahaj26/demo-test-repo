package genericMethods;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import managers.PageObjectRepositoryManager;
import genericMethods.CommonMethods;




/* This class contains all the common methods*/
public class CommonMethods{
	public static WebDriver driver =null;
	public static WebElement element;
	public static PageObjectRepositoryManager ObjRepo = new PageObjectRepositoryManager();
    public By locator;
    
	/***************************************** General Validation  *******************************************/
	//Launch Browser
	

	//Create Web Element
	
	public static WebElement getWebElement(String locatorName, String fileName) throws Exception {
	return element = driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));}
	
	// Clear Element
	public static void clearTextField(WebElement element) {
		element.clear();
	}

	// Click on Element
	public static void clickWebelement(WebElement element) throws Throwable {
		/*try {
			boolean elementIsClickable = element.isEnabled();
			while (elementIsClickable) {
				element.click();
				
			}

		} catch (Exception e) {
			System.out.println("Element is not enabled");
			e.printStackTrace();
		}*/
		element.click();
	}

	// Click on multiple elements
	public static void clickMultipleElements(WebElement someElement, WebElement someOtherElement,WebDriver driver) {
		Actions builder = new Actions(driver);
		builder.keyDown(Keys.CONTROL).click(someElement).click(someOtherElement).keyUp(Keys.CONTROL).build().perform();
	}

	// Highlight element
	public static void highlightelement(WebElement element) {
		for (int i = 0; i < 4; i++) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute(‘style’, arguments[1]);", element,
					"color: solid red; border: 6px solid yellow;");
			js.executeScript("arguments[0].setAttribute(‘style’, arguments[1]);", element, "");
		}
	}

	// To Enter Value
	public void EnterValue(WebElement element, String text) {
		// Lets see how we can find the first name field
		element.sendKeys(text);
	}

	/****************************************
	 * Dropdown Validation
	 ***************************************/
	// Select option by Name
	public static void selectElementByNameMethod(WebElement element, String Name) {
		Select selectitem = new Select(element);
		selectitem.selectByVisibleText(Name);
	}

	// Select option by Value
	public static void selectElementByValueMethod(WebElement element, String value) {
		Select selectitem = new Select(element);
		selectitem.selectByValue(value);
	}

	// Select option by Index
	public static void selectElementByIndexMethod(WebElement element, int index) {
		Select selectitem = new Select(element);
		selectitem.selectByIndex(index);
	}
	//Default selected option
	public static boolean defaultSelectedOption(WebElement element,String defaultSelection) {
		boolean flag =false;
		Select select = new Select(element);
		WebElement option = select.getFirstSelectedOption();
		String selectedOption = option.getText();
	if(selectedOption.contains(defaultSelection)== true) {
		flag = true;
		System.out.println("Dropdown displaying correct default selection");
		
	} else {
		System.out.println("Dropdown displaying incorrect default selection");
	}
	return flag;
	}

	/****************************************
	 * Radio Button and Checkbox Validation
	 ***************************************/
	// CheckBox Validation
	public static void checkbox_Checking(WebElement checkbox) {
		boolean checkstatus;
		checkstatus = checkbox.isSelected();
		if (checkstatus == true) {
			System.out.println("Checkbox is already checked");
		} else {
			checkbox.click();
			System.out.println("Checked the checkbox");
		}
	}

	// Radio button Validation
	public static void radiobutton_Select(WebElement Radio) {
		boolean checkstatus;
		checkstatus = Radio.isSelected();
		if (checkstatus == true) {
			System.out.println("RadioButton is already checked");
		} else {
			Radio.click();
			System.out.println("Selected the Radiobutton");
		}
	}

	// Unchecking Checkbox
	public static void checkbox_Unchecking(WebElement checkbox) {
		boolean checkstatus;
		checkstatus = checkbox.isSelected();
		if (checkstatus == true) {
			checkbox.click();
			System.out.println("Checkbox is unchecked");
		} else {
			System.out.println("Checkbox is already unchecked");
		}
	}

	// Deselect Radio Button
	public static void radioButton_Deselect(WebElement Radio) {
		boolean checkstatus;
		checkstatus = Radio.isSelected();
		if (checkstatus == true) {
			Radio.click();
			System.out.println("Radio Button is deselected");
		} else {
			System.out.println("Radio Button was already Deselected");
		}
	}

	// Click on Checkbox from list
	public static void clickCheckboxFromList(String xpathOfElement, String valueToSelect) {

		List<WebElement> lst = driver.findElements(By.xpath(xpathOfElement));
		for (int i = 0; i < lst.size(); i++) {
			List<WebElement> dr = lst.get(i).findElements(By.tagName("label"));
			for (WebElement f : dr) {
				System.out.println("value in the list : " + f.getText());
				if (valueToSelect.equals(f.getText())) {
					f.click();
					break;
				}
			}
		}
	}

	/****************************************
	 * Screenshot and Time Stamp Validation
	 ***************************************/
	// To Take Screenshot
	public static String fn_TakeSnapshot(WebDriver driver, String DestFilePath) throws IOException {
		String TS = fn_GetTimeStamp();
		TakesScreenshot tss = (TakesScreenshot) driver;
		File srcfileObj = tss.getScreenshotAs(OutputType.FILE);
		DestFilePath = DestFilePath + TS + ".png";
		File DestFileObj = new File(DestFilePath);
		FileUtils.copyFile(srcfileObj, DestFileObj);
		return DestFilePath;
	}

	// For Taking time stamp
	public static String fn_GetTimeStamp() {
		DateFormat DF = DateFormat.getDateTimeInstance();
		Date dte = new Date();
		String DateValue = DF.format(dte);
		DateValue = DateValue.replaceAll(":", "_");
		DateValue = DateValue.replaceAll(",", "");
		return DateValue;
	}

	/****************************************
	 * Window Handler Validation
	 ***************************************/
/*	// To switch to new window
	public static void switchToNewWindow(WebDriver driver) {
		Set s = driver.getWindowHandles();
		Iterator itr = s.iterator();
		String w1 = (String) itr.next();
		String w2 = (String) itr.next();
		driver.switchTo().window(w2);
	}

	// To Switch to old window
	public static void switchToOldWindow(WebDriver driver) {
		Set s = driver.getWindowHandles();
		Iterator itr = s.iterator();
		String w1 = (String) itr.next();
		String w2 = (String) itr.next();
		driver.switchTo().window(w1);
	}*/

	// To switch to Parent window
	public static void switchToParentWindow(WebDriver driver) {
		driver.switchTo().defaultContent();
	}

	// To set size of window
	public static void setWindowSize(int Dimension1, int dimension2) {
		driver.manage().window().setSize(new Dimension(Dimension1, dimension2));
	}

	/****************************************
	 * Wait Validation
	 ***************************************/
	// To wait element to be clickable
	public static void waitForElement(WebElement element, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	// To wait till element found
	public static void waitTillElementFound(WebElement ElementTobeFound, int seconds, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(ElementTobeFound));
	}

	// wait Till Page Load
	public static void waitTillPageLoad(int i) {

		driver.manage().timeouts().pageLoadTimeout(i, TimeUnit.SECONDS);
	}

	// Implicit wait
	public static void waitMyTime(int i) {
		driver.manage().timeouts().implicitlyWait(i, TimeUnit.SECONDS);
	}

	/****************************************
	 * Key Validation
	 ***************************************/
	// To press Down key
	public static void pressKeyDown(WebElement element) {
		element.sendKeys(Keys.DOWN);
	}

	// To press Enter key
	public void pressKeyEnter(WebElement element) {
		element.sendKeys(Keys.ENTER);
	}

	// To press Up key
	public static void pressKeyUp(WebElement element) {
		element.sendKeys(Keys.UP);
	}

	// To Enter through Keyboard
	public static void keyboardEvents(WebElement webelement, Keys key, String alphabet) {
		webelement.sendKeys(Keys.chord(key, alphabet));

	}

	// To Move to Tab
	public static void moveToTab(WebElement element) {
		element.sendKeys(Keys.chord(Keys.ALT, Keys.TAB));
	}

	/****************************************
	 * Multiple Link Validation
	 ***************************************/
	// To Click on All the links present on Page
	public static void clickAllLinksInPage(String destinationOfScreenshot) throws Exception {

		List<WebElement> Links = driver.findElements(By.tagName("a"));
		System.out.println("Total number of links :" + Links.size());

		for (int p = 0; p < Links.size(); p++) {
			System.out.println("Elements present the body :" + Links.get(p).getText());
			Links.get(p).click();
			Thread.sleep(3000);
			System.out.println("Url of the page " + p + ")" + driver.getCurrentUrl());
			fn_TakeSnapshot(driver, destinationOfScreenshot);
			navigate_back();
			Thread.sleep(2000);
		}
	}

	// To Navigate to every link
	public static void navigateToEveryLinkInPage() throws InterruptedException {
		List<WebElement> linksize = driver.findElements(By.tagName("a"));
		int linksCount = linksize.size();
		System.out.println("Total no of links Available: " + linksCount);
		String[] links = new String[linksCount];
		System.out.println("List of links Available: ");
		// print all the links from webpage
		for (int i = 0; i < linksCount; i++) {
			links[i] = linksize.get(i).getAttribute("href");
			System.out.println(linksize.get(i).getAttribute("href"));
		}
		// navigate to each Link on the webpage
		for (int i = 0; i < linksCount; i++) {
			driver.navigate().to(links[i]);
			Thread.sleep(3000);
			System.out.println(driver.getTitle());
		}
	}
    //to get size of list
	public boolean listCount(List<WebElement> ListOfMovies,int refCount) throws InterruptedException {
	int moviesCount = ListOfMovies.size();
	boolean flag =false;
	if(refCount == moviesCount) {
	 flag =true;
	}
	return flag;} 
	/****************************************
	 * Navigation Validation
	 ***************************************/
	// Move Forward
	public static void navigate_forward() {
		driver.navigate().forward();
	}

	// Move backward
	public static void navigate_back() {
		driver.navigate().back();
	}

	// Refresh page
	public static void refresh() {
		driver.navigate().refresh();
	}

	public static void navigateToURL(String URL) {
		driver.navigate().to("arg1");
	}

	/****************************************
	 * Alert Validation
	 ***************************************/
	// Accept Alert Validation
	public static boolean checkAlert_Accept() {
		try {
			Alert a = driver.switchTo().alert();
			String str = a.getText();
			System.out.println(str);

			a.accept();
			return true;

		} catch (Exception e) {

			System.out.println("no alert ");
			return false;

		}
	}

	// Dismiss Alert
	public static boolean checkAlert_Dismiss() {
		try {
			Alert a = driver.switchTo().alert();
			String str = a.getText();
			System.out.println(str);

			a.dismiss();
			return true;

		} catch (Exception e) {

			System.out.println("no alert ");
			return false;

		}
	}

	/****************************************
	 * Scroll Bar Validation
	 ***************************************/
	// Scroll To an element
	public static void scrolltoElement(WebElement ScrolltoThisElement) {
		Coordinates coordinate = ((Locatable) ScrolltoThisElement).getCoordinates();
		coordinate.onPage();
		coordinate.inViewPort();
	}

	/****************************************
	 * Action Class Validation
	 ***************************************/
	public static void dragAndDrop(WebElement fromWebElement, WebElement toWebElement) {
		Actions builder = new Actions(driver);
		builder.dragAndDrop(fromWebElement, toWebElement);
	}

	public static void dragAndDrop_Method2(WebElement fromWebElement, WebElement toWebElement) {
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(fromWebElement).moveToElement(toWebElement).release(toWebElement)
				.build();
		dragAndDrop.perform();
	}

	public static void dragAndDrop_Method3(WebElement fromWebElement, WebElement toWebElement)
			throws InterruptedException {
		Actions builder = new Actions(driver);
		builder.clickAndHold(fromWebElement).moveToElement(toWebElement).perform();
		Thread.sleep(2000);
		builder.release(toWebElement).build().perform();
	}

	// To Hover an element
	public static void hoverWebelement(WebElement HovertoWebElement,WebDriver driver) throws InterruptedException {
		Actions builder = new Actions(driver);
		builder.moveToElement(HovertoWebElement).perform();
		Thread.sleep(2000);
	}

	// Double click Element
	public static void doubleClickWebelement(WebElement doubleclickonWebElement) throws InterruptedException {
		Actions builder = new Actions(driver);
		builder.doubleClick(doubleclickonWebElement).perform();
		Thread.sleep(2000);
	}

	/****************************************
	 * Additional Validation
	 ***************************************/
	public static String getToolTip(WebElement toolTipofWebElement) throws InterruptedException {
		String tooltip = toolTipofWebElement.getAttribute("title");
		System.out.println("Tool text : " + tooltip);
		return tooltip;
	}

	// Download a file
	public static void downloadFile(String href, String fileName) throws Exception {
		URL url = null;
		URLConnection con = null;
		int i;
		url = new URL(href);
		con = url.openConnection();
		File file = new File(".//OutputData//" + fileName);
		BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
		while ((i = bis.read()) != -1) {
			bos.write(i);
		}
		bos.flush();
		bis.close();
		bos.close();
	}

	// Verify Page Title
	public static boolean verifyPageTitle(String refTitle,WebDriver driver) {
		  String str = driver.getTitle();
		  boolean flag= false;
		  if(str.contains(refTitle)) {
			  flag =true;
		  }
		  return flag;
		  }
	
	// Assert Page title
	public boolean VerifyPageTitle(String Expected_Window_Name, WebDriver driver) {
		String Expected_Title = Expected_Window_Name;

		for (String Handle : driver.getWindowHandles()) {
			String Actual_Title = driver.switchTo().window(Handle).getTitle();

			if (Actual_Title.startsWith(Expected_Title)) {
				driver.switchTo().window(Handle);
				return true;
			}

		}

		return false;
	}

	// Verify the element displayed
	public boolean VerifyIsDisplayed(WebElement element, WebDriver driver) {
		if (element.isDisplayed()) {
			return true;
		} else
			return false;
	}
}