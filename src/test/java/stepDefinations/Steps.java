package stepDefinations;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import PageJava.TestCode;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.PageObjectRepositoryManager;
import utility.CreateSheetAndStoreDate;
import assertAndVerify.AssertionClass;
import genericMethods.CommonMethods;




public class Steps{
	public static WebDriver driver;
	public static WebElement element;
	public static PageObjectRepositoryManager ObjRepo = new PageObjectRepositoryManager();	
	public CommonMethods cm = new CommonMethods();
    public By locator;
    public static AssertionClass assertion;
    public CreateSheetAndStoreDate csas = new CreateSheetAndStoreDate();
    public static TestCode tc = new TestCode(); 
    
    //Cucumber Hooks
    @Before
    public void launchChrome(){
    	System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver.exe");
	    driver = new ChromeDriver();}
    
    @After
    public void closeChrome(){
    	driver.close(); 
    }
    
  //Launch URL
    
  	public void launchURL(String url) {
  	//driver.manage().window().maximize();
  	//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  	driver.get(url);}
  /*************************************Step Definitions******************************/  
   	@Given("^User launch \"([^\"]*)\" URL$")
    	public void LaunchURL(String url) throws Exception {
   		launchURL(url);
	}
   	
   @When("^User mouse hover on \"([^\"]*)\" field on \"([^\"]*)\" page$")  
       public void mouseHover(String locatorName, String fileName) throws Throwable {
	   element = driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));
	   CommonMethods.hoverWebelement(element,driver);
    }
   
   @And("^User find and click on \"([^\"]*)\" link in \"([^\"]*)\" available List on \"([^\"]*)\" page$")
    public void findAndClickDesiredLink(String reqOption,String locatorName, String fileName) throws Throwable{
	   element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));
	   CommonMethods.clickWebelement(element);
		}
   
   @And("^User validate \"([^\"]*)\" is visible on \"([^\"]*)\" page$")
   public void verifyText(String locatorName, String fileName) throws Throwable{
	   element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));
	   AssertionClass.assertTrue("Top 250 as rated by IMDb Users byline is visible", cm.VerifyIsDisplayed(element,driver));
		}
   
@And("^User validate the \"([^\"]*)\" default value of \"([^\"]*)\" dropdown on \"([^\"]*)\" page$")
   public void user_validate_the_default_value_of_dropdown_on_page(String defaultOption, String locatorName, String fileName) throws Exception {
	   element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));
	   AssertionClass.assertTrue("Default selected option is Ranking", CommonMethods.defaultSelectedOption(element,defaultOption)); 
       
   }

   @And("^User validate the \"([^\"]*)\" default order of list on \"([^\"]*)\" page$")
   public void user_validate_the_default_order_of_list_on_page(String locatorName, String fileName) throws Throwable {
	   element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName)); 
	   AssertionClass.assertTrue("Element Displayed on page",cm.VerifyIsDisplayed(element, driver));
       
   }

   @And("^User validate the count of \"([^\"]*)\" list should be equal to \"([^\"]*)\" on \"([^\"]*)\" page$")
   public void user_validate_the_count_of_list_should_be_equal_to_on_page(String locatorName,int refCount, String fileName) throws Exception {
		List<WebElement> ListOfMovies = driver.findElements(ObjRepo.getObjectLocator(locatorName, fileName));  
		 AssertionClass.assertTrue("Count is same",cm.listCount(ListOfMovies,refCount));
   }
			
  @When("^User fetch the \"([^\"]*)\" movie titles with \"([^\"]*)\" year of release and \"([^\"]*)\" rating in sorted order of \"([^\"]*)\" on \"([^\"]*)\" page$")
		public void getDetails(String locatorName1, String locatorName2, String locatorName3, String filter, String fileName) throws Exception {
		// Write code here that turns the phrase above into concrete actions
	  List<WebElement> ListOfTitle = driver.findElements(ObjRepo.getObjectLocator(locatorName1, fileName));
	  List<WebElement> ListOfReleaseYear = driver.findElements(ObjRepo.getObjectLocator(locatorName2, fileName));
	  List<WebElement> ListOfImdbRating = driver.findElements(ObjRepo.getObjectLocator(locatorName3, fileName));
	  TestCode.fetchAndStoreListOfMovies(ListOfTitle, ListOfReleaseYear, ListOfImdbRating, filter);
   }
  @Then("^User validate the filter in Descending order of \"([^\"]*)\" field on \"([^\"]*)\" page$")
  public void validateDescOrderOfDisplay(String locatorName,String fileName) throws Exception {
	  List<WebElement> ListOfImdbRating = driver.findElements(ObjRepo.getObjectLocator(locatorName, fileName)); 
	  AssertionClass.assertTrue("movies are displaying in desc order of ranking", TestCode.verifyDisplayOrderRankingDesc(ListOfImdbRating));
  }

  @When("^User find and click on \"([^\"]*)\" on \"([^\"]*)\" page$")
  public void launchFromFooter(String locatorName, String fileName) throws Throwable {
	  element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName));
	  CommonMethods.clickWebelement(element);
  }
   
  @Then("^User validate the filter in Ascending order of \"([^\"]*)\" field on \"([^\"]*)\" page$")
  public void validateAscOrderOfDisplay(String locatorName,String fileName) throws Exception {
	  List<WebElement> ListOfImdbRating = driver.findElements(ObjRepo.getObjectLocator(locatorName, fileName)); 
	  AssertionClass.assertTrue("movies are displaying in desc order of ranking", TestCode.verifyDisplayOrderRankingAscending(ListOfImdbRating));
  } 
  
  @Then("^User redirected to new page and validate page title displayed as \"([^\"]*)\"$")
  public static void verifyTitle(String refTitle) {
	  AssertionClass.assertTrue("Page tile is correct", CommonMethods.verifyPageTitle(refTitle,driver));	  
  }
  

@When("^User change the \"([^\"]*)\" order of list on \"([^\"]*)\" page$")
public void changeOrder(String locatorName, String fileName) throws Throwable {
	element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName)); 
	element.click();
    
}

@When("^User select \"([^\"]*)\" option from \"([^\"]*)\" dropdown on \"([^\"]*)\" page$")
public void user_select_option_from_dropdown_on_page(String Name, String locatorName, String fileName) throws Throwable {
	element =driver.findElement(ObjRepo.getObjectLocator(locatorName, fileName)); 
	CommonMethods.selectElementByNameMethod(element,Name);
    
}

@Then("^User validate the filter in ascending order of \"([^\"]*)\" year of release on \"([^\"]*)\" page$")
public void validatReleaseYearAscOrderOfDisplay(String locatorName,String fileName) throws Exception {
	  List<WebElement> ListOfReleaseYear = driver.findElements(ObjRepo.getObjectLocator(locatorName, fileName)); 
	  AssertionClass.assertTrue("movies are displaying in desc order of ranking", TestCode.verifyDisplayOrderYearAsc(ListOfReleaseYear));
} 

@Then("^User validate the filter in Descending order of \"([^\"]*)\" year of release on \"([^\"]*)\" page$")
public void validatReleaseYearDescOrderOfDisplay(String locatorName,String fileName) throws Exception {
	  List<WebElement> ListOfReleaseYear = driver.findElements(ObjRepo.getObjectLocator(locatorName, fileName)); 
	  AssertionClass.assertTrue("movies are displaying in desc order of ranking", TestCode.verifyDisplayOrderYearDesc(ListOfReleaseYear));
} 
}

   	

	


