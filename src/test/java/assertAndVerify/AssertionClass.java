package assertAndVerify;

import org.junit.Assert;

public class AssertionClass {
	/* Assertion methods */
	
	//Asset Equals
	public static void assertEquals(String expected, String actual) {
		Assert.assertEquals(expected, actual);
	}

	//assertTrue
	public static void assertTrue(java.lang.String message, boolean condition) {
		/*message – Message to be displayed in case of an Assertion Error.
          condition – Condition against which the assertion needs to be applied.*/
		Assert.assertTrue(message, true);
	}
	
	//assertFalse
	public static void assertFalse(java.lang.String message, boolean condition) {
		/*message – Message to be displayed in case of an Assertion Error.
          condition – Condition against which the assertion needs to be applied.*/
		Assert.assertFalse(message, false);
	}
	
	//assertNull
	public static void assertNull(Object object) {
		// to verify if the provided object contains a null value.
		Assert.assertNull(object);
	}
	
	// assertNotNull
	public static void assertNotNull(Object object) {
		// to verify if the provided object doesn't contains a null value.
		Assert.assertNotNull(object);
	}
	
	//assertSame
	public static void assertSame(String message, Object expected,Object actual) {
		Assert.assertSame(message, expected, actual);
	}
	
	//assertSame
		public static void assertNotSame(String message, Object expected,Object actual) {
			Assert.assertNotSame(message, expected, actual);
		}
   //assertArrayEquals
      public static void assertArrayEquals(String message, Object[] expected, Object[] actual) {
    	  Assert.assertArrayEquals(expected,actual);
      }
      
      
}
