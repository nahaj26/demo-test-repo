package PageJava;

import java.io.IOException;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;

import utility.CreateSheetAndStoreDate;

public class TestCode{
	
	/*specific code for page object model*/
	
	public static void fetchAndStoreListOfMovies(List<WebElement> ListOfTitle,List<WebElement> ListOfReleaseYear, List<WebElement> ListOfImdbRating ,String filter) throws InvalidFormatException, IOException {
		
	int moviesCount = ListOfTitle.size();
	  for (int i = 0; i < moviesCount; i++) {
		  String Title = ListOfTitle.get(i).getText();
		  String Year = ListOfReleaseYear.get(i).getText();
		  String Rating = ListOfImdbRating.get(i).getText();
		  Year = Year.replaceAll("[\\(\\)\\[\\]\\{\\}]","");
		  CreateSheetAndStoreDate.storeData(Title, Year, Rating);
		}
	  CreateSheetAndStoreDate.createExcel(filter);
}
	//To check where the list is sorting in descending order of ranking
	public static boolean verifyDisplayOrderRankingDesc(List<WebElement> ListOfImdbRating) throws InvalidFormatException, IOException {
		boolean flag =false;
		int moviesCount = ListOfImdbRating.size();
		  for (int i = 0; i < moviesCount-1; i++) {
			  String Rating1 = ListOfImdbRating.get(i).getText();
			  String Rating2 = ListOfImdbRating.get(i+1).getText();
	          float r1 = Float.parseFloat(Rating1);
	          float r2 = Float.parseFloat(Rating2);
	          if (r1>=r2) {
	            flag = true;}
	          }
		return flag;
         }
	
	//To check where the list is sorting in ascending order of ranking
	public static boolean verifyDisplayOrderRankingAscending(List<WebElement> ListOfImdbRating) throws InvalidFormatException, IOException {
		boolean flag =false;
		int moviesCount = ListOfImdbRating.size();
		  for (int i = 0; i < moviesCount-1; i++) {
			  String Rating1 = ListOfImdbRating.get(i).getText();
			  String Rating2 = ListOfImdbRating.get(i+1).getText();
	          float r1 = Float.parseFloat(Rating1);
	          float r2 = Float.parseFloat(Rating2); 
	          if (r1<=r2) {
	            flag = true;}
	          }
		return flag;
         }
	

	//To check where the list is sorting in descending order of year of release
	public static boolean verifyDisplayOrderYearDesc(List<WebElement> ListOfReleaseYear) throws InvalidFormatException, IOException {
		boolean flag =false;
		int moviesCount = ListOfReleaseYear.size();
		  for (int i = 0; i < moviesCount-1; i++) {
			  String year1 = ListOfReleaseYear.get(i).getText();
			  String year2 = ListOfReleaseYear.get(i+1).getText();
			  year1 = year1.replaceAll("[\\(\\)\\[\\]\\{\\}]","");
			  year2 = year2.replaceAll("[\\(\\)\\[\\]\\{\\}]","");
			  
	          float r1 = Float.parseFloat(year1);
	          float r2 = Float.parseFloat(year2); 
	          if (r1>=r2) {
	            flag = true;}
	          }
		return flag;
         }
	
	//To check where the list is sorting in ascending order of year of release
	public static boolean verifyDisplayOrderYearAsc(List<WebElement> ListOfReleaseYear) throws InvalidFormatException, IOException {
		boolean flag =false;
		int moviesCount = ListOfReleaseYear.size();
		  for (int i = 0; i < moviesCount-1; i++) {
			  String year1 = ListOfReleaseYear.get(i).getText();
			  String year2 = ListOfReleaseYear.get(i+1).getText();
			  year1 = year1.replaceAll("[\\(\\)\\[\\]\\{\\}]","");
			  year2 = year2.replaceAll("[\\(\\)\\[\\]\\{\\}]","");
			  
	          float r1 = Float.parseFloat(year1);
	          float r2 = Float.parseFloat(year2);
	          if (r1<=r2) {
	            flag = true;}
	          }
		return flag;
         }
	
	 
 
	
}