
Feature: E2E test
Description: E2E Test Scenarios
Scenario: IMDB Top Rated Movies default list validation on launching from navigation menu in descending order of ranking
Given User launch "https://www.imdb.com/" URL
When User mouse hover on "MoviesTvShowtime" field on "imdb" page
And User find and click on "Top Rated Movies" link in "ListOfOption" available List on "imdb" page
Then User redirected to new page and validate page title displayed as "IMDb Top 250 - IMDb"
And User validate "Byline" is visible on "imdb" page
And User validate the "Ranking" default value of "SortByDropdown" dropdown on "imdb" page
And User validate the "DefaultDisplayOrder" default order of list on "imdb" page
And User validate the count of "MovieList" list should be equal to "250" on "imdb" page
When User fetch the "TitleList" movie titles with "ReleaseYearList" year of release and "RatingList" rating in sorted order of "SortByRankingDrfault" on "imdb" page
Then User validate the filter in Descending order of "RatingList" field on "imdb" page

Scenario: IMDB Top Rated Movies default list validation on launching from footer link
Given User launch "https://www.imdb.com/" URL 
When User find and click on "FooterLink" on "imdb" page
Then User redirected to new page and validate page title displayed as "IMDb Top 250 - IMDb"
And User validate "Byline" is visible on "imdb" page
And User validate the "Ranking" default value of "SortByDropdown" dropdown on "imdb" page
And User validate the "DefaultDisplayOrder" default order of list on "imdb" page
And User validate the count of "MovieList" list should be equal to "250" on "imdb" page
When User fetch the "TitleList" movie titles with "ReleaseYearList" year of release and "RatingList" rating in sorted order of "SortByRankingDesc" on "imdb" page
Then User validate the filter in Descending order of "RatingList" field on "imdb" page

Scenario: IMDB Top Rated Movies list validation on launching from navigation menu in ascending order of ranking
Given User launch "https://www.imdb.com/" URL
When User mouse hover on "MoviesTvShowtime" field on "imdb" page
And User find and click on "Top Rated Movies" link in "ListOfOption" available List on "imdb" page
Then User redirected to new page and validate page title displayed as "IMDb Top 250 - IMDb"
And User validate "Byline" is visible on "imdb" page
And User validate the "Ranking" default value of "SortByDropdown" dropdown on "imdb" page
And User validate the "DefaultDisplayOrder" default order of list on "imdb" page
When User change the "DefaultDisplayOrder" order of list on "imdb" page
Then User validate the count of "MovieList" list should be equal to "250" on "imdb" page
When User fetch the "TitleList" movie titles with "ReleaseYearList" year of release and "RatingList" rating in sorted order of "SortByRankingAsc" on "imdb" page
Then User validate the filter in Ascending order of "RatingList" field on "imdb" page

Scenario: IMDB Top Rated Movies validation on launching from navigation menu in descending order of Year of Release
Given User launch "https://www.imdb.com/" URL
When User mouse hover on "MoviesTvShowtime" field on "imdb" page
And User find and click on "Top Rated Movies" link in "ListOfOption" available List on "imdb" page
Then User redirected to new page and validate page title displayed as "IMDb Top 250 - IMDb"
And User validate "Byline" is visible on "imdb" page
When User select "Release Date" option from "SortByDropdown" dropdown on "imdb" page
Then User validate the "DefaultDisplayOrder2" default order of list on "imdb" page
And User validate the count of "MovieList" list should be equal to "250" on "imdb" page
When User fetch the "TitleList" movie titles with "ReleaseYearList" year of release and "RatingList" rating in sorted order of "SortByYearDesc" on "imdb" page
Then User validate the filter in Descending order of "ReleaseYearList" year of release on "imdb" page

Scenario: IMDB Top Rated Movies validation on launching from navigation menu in ascending order of Year of Release
Given User launch "https://www.imdb.com/" URL
When User mouse hover on "MoviesTvShowtime" field on "imdb" page
And User find and click on "Top Rated Movies" link in "ListOfOption" available List on "imdb" page
Then User redirected to new page and validate page title displayed as "IMDb Top 250 - IMDb"
And User validate "Byline" is visible on "imdb" page
When User select "Release Date" option from "SortByDropdown" dropdown on "imdb" page
Then User validate the "DefaultDisplayOrder2" default order of list on "imdb" page
When User change the "DefaultDisplayOrder2" order of list on "imdb" page
Then User validate the count of "MovieList" list should be equal to "250" on "imdb" page
When User fetch the "TitleList" movie titles with "ReleaseYearList" year of release and "RatingList" rating in sorted order of "SortByYearAsc" on "imdb" page
Then User validate the filter in ascending order of "ReleaseYearList" year of release on "imdb" page



 